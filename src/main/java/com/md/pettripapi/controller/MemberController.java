package com.md.pettripapi.controller;

import com.md.pettripapi.model.member.MemberCreateRequest;
import com.md.pettripapi.model.member.MemberItem;
import com.md.pettripapi.model.member.MemberPhoneNumberChangeRequest;
import com.md.pettripapi.model.member.MemberResponse;
import com.md.pettripapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")
    public String setMember(@RequestBody MemberCreateRequest request){
        memberService.setMember(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers(){
        return memberService.getMembers();
    }

    @GetMapping("/detail/{id}")
    public MemberResponse getMember(@PathVariable long id){
        return memberService.getMember(id);
    }

    @PutMapping("/phone-number/{id}")
    public String putMemberPhoneNumber(@PathVariable long id, @RequestBody MemberPhoneNumberChangeRequest request){
        memberService.putMemberPhoneNumber(id, request);

        return "ok";
    }

    @DeleteMapping("/del-member/{id}")
    public String delMember(@PathVariable long id){
        memberService.delMember(id);

        return "ok";
    }
}
