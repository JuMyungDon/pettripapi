package com.md.pettripapi.model.pet;

import com.md.pettripapi.enums.Gender;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetCreateRequest {
    private String animal;
}
