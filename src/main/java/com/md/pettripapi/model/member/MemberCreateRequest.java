package com.md.pettripapi.model.member;

import com.md.pettripapi.enums.Gender;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberCreateRequest {
    private String name;
    private String phoneNumber;
    @Enumerated(value = EnumType.STRING)
    private Gender gender;
}
