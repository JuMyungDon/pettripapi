package com.md.pettripapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberPhoneNumberChangeRequest {
    private String phoneNumber;
}
