package com.md.pettripapi.model.member;

import com.md.pettripapi.enums.Gender;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberItem {
    private Long id;
    private String name;
    private String phoneNumber;
    private String gender;
}
