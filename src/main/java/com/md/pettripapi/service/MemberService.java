package com.md.pettripapi.service;

import com.md.pettripapi.entity.Member;
import com.md.pettripapi.model.member.MemberCreateRequest;
import com.md.pettripapi.model.member.MemberItem;
import com.md.pettripapi.model.member.MemberPhoneNumberChangeRequest;
import com.md.pettripapi.model.member.MemberResponse;
import com.md.pettripapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id){
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request){
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setGender(request.getGender());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers(){
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        for (Member member : originList){
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setName(member.getName());
            addItem.setPhoneNumber(member.getPhoneNumber());
            addItem.setGender(member.getGender().getName());

            result.add(addItem);
        }

        return result;
    }

    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();

        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setGender(originData.getGender().getName());

        return response;
    }

    public void putMemberPhoneNumber(long id, MemberPhoneNumberChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setPhoneNumber(request.getPhoneNumber());

        memberRepository.save(originData); 
    }

    public void delMember(long id){
        memberRepository.deleteById(id);
    }
}
