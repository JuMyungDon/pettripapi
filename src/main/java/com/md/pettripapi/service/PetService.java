package com.md.pettripapi.service;

import com.md.pettripapi.entity.Member;
import com.md.pettripapi.entity.Pet;
import com.md.pettripapi.model.pet.PetCreateRequest;
import com.md.pettripapi.repository.MemberRepository;
import com.md.pettripapi.repository.PetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PetService {
    private final PetRepository petRepository;

    public void setPet(Member member, PetCreateRequest request){
        Pet addData = new Pet();
        addData.setMember(member);
        addData.setAnimal(request.getAnimal());

        petRepository.save(addData);
    }
}
